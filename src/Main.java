import solution.Solution;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    // final Object mutex = new Object();
    static AtomicInteger evaluationNumber = new AtomicInteger(0);
    final static List<String> genericConfigurationFile = new ArrayList<>();

    static String workingDirectory = System.getProperty("user.dir");
    static String currentSimulationFolderPath = workingDirectory + File.separator + new Timestamp(System.currentTimeMillis()).getTime();
    static String configFolderPath = currentSimulationFolderPath + File.separator + "configs";
    static String genericConfigFilePath = workingDirectory + File.separator + "generic.cfg";
    static String resultsFolderPath =  currentSimulationFolderPath + File.separator + "results";
    static String simulatorPath = workingDirectory + File.separator + "simulator";

    public static void main(String[] args) {

        try {
            readGenericConfigurationFile();
            createFolders();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        int numberOfGenes = 3;
        int numberOfObjectives = 1;

        System.out.println("Enter the log 2 upper limit for associativity: ");
        Scanner scanner = new Scanner(System.in);
        int associativityUpperLimit = scanner.nextInt();

        System.out.println("Enter the log 2 upper limit for DIR table dimension: ");
        int tableDimensionUpperLimit = scanner.nextInt();

        System.out.println("Enter the number of iterations: ");
        int numberOfIterations = scanner.nextInt();

        int currentIteration = 0;
        Solution bestSolution = new Solution(numberOfGenes, numberOfObjectives);
        bestSolution.getObjectives()[0] = Double.MAX_VALUE;
        while (currentIteration < numberOfIterations) {
            boolean local = false;

            Solution solution = new Solution(numberOfGenes, numberOfObjectives);
            try {
                solution.setGeneMetadata(associativityUpperLimit, tableDimensionUpperLimit);
                solution.generateRandomGenes();
                evaluateSolution(solution, currentIteration);
                while (!local) {
                    Solution[] neighbourSolutions = solution.getNeighbours();
                    for (Solution neighbourSolution : neighbourSolutions) {
                        evaluateSolution(neighbourSolution, currentIteration);
                    }
                    Solution bestNeighbourSolution = getBestSolution(neighbourSolutions);
                    if (bestNeighbourSolution.getObjectives()[0] >  solution.getObjectives()[0]) {
                        solution = bestNeighbourSolution;
                    } else {
                        local = true;
                    }
                }
                currentIteration++;
                if (solution.getObjectives()[0] > bestSolution.getObjectives()[0]) {
                    bestSolution = solution;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return;
            }
        }
    }

    private static void createFolders() {
        createFolder(currentSimulationFolderPath);
        createFolder(configFolderPath);
        createFolder(resultsFolderPath);
    }

    private static void createFolder(String path) {
        if (!Files.exists(Paths.get(path))) {
            try {
                Files.createDirectory(Paths.get(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void readGenericConfigurationFile() throws Exception {
        if (Files.exists(Paths.get(genericConfigFilePath))) {
            Scanner scanner;
            try {
                scanner = new Scanner(new File(genericConfigFilePath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
            while (scanner.hasNextLine()) {
                genericConfigurationFile.add(scanner.nextLine());
            }
        } else {
            throw new Exception("Could not find the generic configuration file");
        }
    }

    private static Solution getBestSolution(Solution[] neighbourSolutions) {
        Solution bestSolution = neighbourSolutions[0];
        for (Solution neighbourSolution : neighbourSolutions) {
            if (neighbourSolution.getObjectives()[0] < bestSolution.getObjectives()[0]) {
                bestSolution = neighbourSolution;
            }
        }
        return bestSolution;
    }

    private static void evaluateSolution(Solution solution, int iteration) {
        try {
            int evaluation = evaluationNumber.getAndIncrement();
            String configurationFilePath = composeConfigurationFilePath(iteration, evaluation);
            List<String> concreteFile = composeConcreteConfigurationFile(solution);
            writeToFile(configurationFilePath, concreteFile);
            String currentSolutionFolderPath = resultsFolderPath + File.separator + "ITERATION_" + iteration + "_EVALUATION_" + evaluation;
            createFolder(currentSolutionFolderPath);
            simulate(currentSolutionFolderPath, configurationFilePath);
            solution.getObjectives()[0] = extractResults(currentSolutionFolderPath);
        } catch (Exception exception) {
            System.out.println("Could not evaluate solution in iteration " + iteration);
            System.out.println(solution.toString());
            System.out.println(exception.getMessage());
        }

    }

    private static double extractResults(String currentSolutionFolderPath) {
        Scanner reusePercentageScanner = null;
        double totalReusePercentage = 0d;
        int counter = 0;
        String simOutPath = currentSolutionFolderPath + File.separator + "sim.out";
        try {
            if (Files.exists(Paths.get(simOutPath))) {
                reusePercentageScanner = new Scanner(new File(simOutPath));
                String line = null;
                while (reusePercentageScanner.hasNextLine()) {
                    line = reusePercentageScanner.nextLine();
                    if (!line.contains("reuse percentage")) {
                        continue;
                    }
                    line = line.trim();

                    String[] split = line.split("\\s+");
                    for (int i = 0; i < split.length; i++) {
                        if (tryParseDouble(split[i]) != null) {
                            counter++;
                            totalReusePercentage += tryParseDouble(split[i]);
                        }
                    }
                    break;
                }
                reusePercentageScanner.close();
            }
            if (counter > 0) {
                return totalReusePercentage / counter;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    static Double tryParseDouble(String value) {
        try {
            double result;
            result = Double.parseDouble(value);
            return result;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static void simulate(String solutionDirectory, String configurationFilePath) {
        ProcessBuilder pb = new ProcessBuilder("/bin/bash", simulatorPath + File.separator +"lerun_test.sh", simulatorPath, solutionDirectory, "4", configurationFilePath, "splash2-barnes");
        Process process = null;
        try {
            process = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            StringBuilder output = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                //output.append(line).append("\n");
                System.out.println(line);
            }
            process.waitFor();
            process.destroy();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void writeToFile(String filePath, List<String> fileContents) {
        try {
            FileWriter myWriter = new FileWriter(filePath);
            for (int i = 0; i < fileContents.size(); i++) {
                myWriter.write(fileContents.get(i));
                myWriter.write('\n');
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred when writing to file!");
            e.printStackTrace();
        }
    }

    private static List<String> composeConcreteConfigurationFile(Solution solution) throws Exception {
        List<String> concreteFile = new ArrayList<>();
        for (int i = 0; i < genericConfigurationFile.size(); i++) {
            String unmodifiedString = genericConfigurationFile.get(i);
            boolean addUnmodifiedString = true;
            if (unmodifiedString.contains("{") && unmodifiedString.contains("}")) {
                for (int j = 0; j < solution.getNumberOfGenes(); j++) {
                    String placeholder = "{" + solution.getGenes()[j].getGeneName() + "}";
                    if (unmodifiedString.contains(placeholder)) {
                        Integer replaceValue = solution.getGenes()[j].getActualGeneValue();
                        String newString = unmodifiedString.replace(placeholder, replaceValue.toString());
                        addUnmodifiedString = false;
                        concreteFile.add(newString);
                    }
                }
            }
            if (addUnmodifiedString) {
                concreteFile.add(unmodifiedString);
            }
        }
        return concreteFile;
    }

    private static String composeConfigurationFilePath(int iteration, int evaluation) {
        return configFolderPath + File.separator + "ITERATION_" + iteration + "_EVALUATION_" + evaluation + ".cfg";
    }
}
