package solution;

public class Gene {
    public Integer geneValue;
    public Integer upperLimit;
    public Integer lowerLimit;
    public Boolean isExponential;
    public String geneName;

    public Gene() {
        geneName = null;
        geneValue = null;
        upperLimit = null;
        lowerLimit = null;
        isExponential = null;
    }

    public Integer getGeneValue() {
        return geneValue;
    }

    public void setGeneValue(Integer geneValue) {
        this.geneValue = geneValue;
    }

    public Integer getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(Integer upperLimit) {
        this.upperLimit = upperLimit;
    }

    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Integer lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public Boolean getExponential() {
        return isExponential;
    }

    public void setExponential(Boolean exponential) {
        isExponential = exponential;
    }

    public String getGeneName() {
        return geneName;
    }

    public void setGeneName(String geneName) {
        this.geneName = geneName;
    }

    public Gene copy() {
        Gene copy = new Gene();
        copy.setGeneValue(geneValue);
        copy.setGeneName(geneName);
        copy.setUpperLimit(upperLimit);
        copy.setLowerLimit(lowerLimit);
        copy.setExponential(isExponential);
        return copy;
    }

    public Integer getActualGeneValue() throws Exception {
        if (isExponential == null) {
            throw new Exception("isExponential is not set for gene!");
        }
        if (isExponential) {
            return (int) Math.pow(2, geneValue);
        }
        return geneValue;
    }
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n\t{\n\t\tgeneValue: ").append(geneValue).append("\n");
        buffer.append("\t\tupperLimit: ").append(upperLimit).append("\n");
        buffer.append("\t\tlowerLimit: ").append(lowerLimit).append("\n");
        buffer.append("\t\tisExponential: ").append(isExponential).append("\n");
        buffer.append("\t\tgeneName: ").append(geneName).append("\n\t}");
        return buffer.toString();
    }
}
