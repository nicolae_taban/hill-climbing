package solution;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    int numberOfGenes;
    int numberOfObjectives;
    Gene[] genes;
    double[] objectives;

    public Solution(int numberOfGenes, int numberOfObjectives) {
        this.numberOfGenes = numberOfGenes;
        this.numberOfObjectives = numberOfObjectives;

        genes = new Gene[numberOfGenes];
        for (int i = 0; i < numberOfGenes; i++) {
            genes[i] = new Gene();
        }

        objectives = new double[numberOfObjectives];
        for (int i = 0; i < numberOfObjectives; i++) {
            objectives[i] = 0;
        }
    }

    public int getNumberOfObjectives() {
        return numberOfObjectives;
    }

    public void setNumberOfObjectives(int numberOfObjectives) {
        this.numberOfObjectives = numberOfObjectives;
    }

    public double[] getObjectives() {
        return objectives;
    }

    public void setObjectives(double[] objectives) {
        this.objectives = objectives;
    }

    public int getNumberOfGenes() {
        return numberOfGenes;
    }

    public void setNumberOfGenes(int numberOfGenes) {
        this.numberOfGenes = numberOfGenes;
    }

    public Gene[] getGenes() {
        return genes;
    }

    public void setGenes(Gene[] genes) {
        this.genes = genes;
    }

    public void generateRandomGenes() throws Exception {
        for (int i = 0; i < numberOfGenes; i++) {
            Gene gene = genes[i];
            if (gene.getLowerLimit() == null || gene.getUpperLimit() == null) {
                throw new Exception("Gene " + i + " " + gene.getGeneName() + " has null limit: lower limit " + gene.getLowerLimit() + " upper limit " + gene.getUpperLimit());
            }
            int randomGeneValue = (int) (gene.getLowerLimit() + (Math.random() * (gene.getUpperLimit() - gene.getLowerLimit())));
            gene.setGeneValue(randomGeneValue);
        }
    }

    private boolean geneDoesNotExist(int geneNumber) {
        return geneNumber < 0 || geneNumber > numberOfGenes;
    }

    public void setGeneBounds(int geneNumber, int lowerLimit, int upperLimit) throws Exception {
        if (geneDoesNotExist(geneNumber)) {
            throw new Exception("Gene " + geneNumber + " does not exist!");
        }
        genes[geneNumber].setLowerLimit(lowerLimit);
        genes[geneNumber].setUpperLimit(upperLimit + 1);
    }

    public void setGeneName(int geneNumber, String geneName) throws Exception {
        if (geneDoesNotExist(geneNumber)) {
            throw new Exception("Gene " + geneNumber + " does not exist!");
        }
        genes[geneNumber].setGeneName(geneName);
    }

    public void setGeneIsExponential(int geneNumber, boolean isExponential) throws Exception {
        if (geneDoesNotExist(geneNumber)) {
            throw new Exception("Gene " + geneNumber + " does not exist!");
        }
        genes[geneNumber].setExponential(isExponential);
    }

    public void setGeneMetadata(int associativityUpperLimit, int tableDimensionUpperLimit) throws Exception {
        setGeneBounds(0, 0, associativityUpperLimit);
        setGeneName(0, "DIR_ASSOCIATIVITY");
        setGeneIsExponential(0, true);

        setGeneBounds(1, 0, tableDimensionUpperLimit);
        setGeneName(1, "DIR_SETS");
        setGeneIsExponential(1, true);

        setGeneBounds(2, 1, 2);
        setGeneName(2, "DIR_REPLACEMENT_POLICY");
        setGeneIsExponential(2, false);
    }

    public Solution[] getNeighbours() {
        List<Solution> solutionList = new ArrayList<>();
        for (int i = 0; i < numberOfGenes; i++) {
            if (genes[i].getGeneValue() < genes[i].getUpperLimit() - 1) {
                Solution copy = copy();
                Integer geneValue = copy.genes[i].getGeneValue();
                copy.genes[i].setGeneValue(++geneValue);
                solutionList.add(copy);
            }
            if (genes[i].getGeneValue() > genes[i].getLowerLimit()) {
                Solution copy = copy();
                Integer geneValue = copy.genes[i].getGeneValue();
                copy.genes[i].setGeneValue(--geneValue);
                solutionList.add(copy);
            }
        }
        Solution[] result = new Solution[solutionList.size()];
        solutionList.toArray(result);
        return result;
    }

    private Solution copy() {
        Solution copy = new Solution(numberOfGenes, numberOfObjectives);
        for (int i = 0; i < numberOfGenes; i++) {
            copy.genes[i] = genes[i].copy();
        }
        return copy;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("{\n\tnumberOfGenes: ").append(numberOfGenes).append("\n\tnumberOfObjectives: ").append(numberOfObjectives);
        for (int i = 0; i < numberOfGenes; i++) {
            buffer.append(genes[i].toString());
        }
        for (int i = 0; i < numberOfObjectives; i++) {
            buffer.append("\n\tobjective ").append(i).append(": ").append(objectives[i]);
        }
        buffer.append("\n}");
        return buffer.toString();
    }
}
